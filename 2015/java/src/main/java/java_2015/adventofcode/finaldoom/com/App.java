package java_2015.adventofcode.finaldoom.com;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Field;
import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.OptionalInt;
import java.util.Scanner;
import java.util.TreeMap;
import java.util.function.BinaryOperator;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import org.apache.commons.codec.digest.DigestUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONTokener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class App implements AdventOfCode {
    private static final Logger LOG = LoggerFactory.getLogger(App.class);
    private static final int START_FLOOR = 0;

    public static void main(String[] args) {
        new App().doDay19Part2();
    }

    public void doDay1() {
        int floor = START_FLOOR;
        int position = 0;
        int basementPosition = 0;
        try (InputStream in = InputLoader.getInput(1)) {
            while (in.available() > 0) {
                char instruction = (char) in.read();
                switch (instruction) {
                    case '(':
                        floor++;
                        break;
                    case ')':
                        floor--;
                        break;
                    default:
                        LOG.warn("Unknown input: " + instruction);
                }
                position++;
                if (basementPosition <= 0 && floor < 0) {
                    basementPosition = position;
                }
            }
        } catch (IOException e) {
            LOG.error("Problem with input", e);
            throw new RuntimeException("Problem with input", e);
        }
        System.out.println("Santa reached floor " + floor);
        System.out.println("Santa first entered the basement at position " + basementPosition);
    }

    public void doDay2() {
        int paperTotal = 0;
        int ribbonTotal = 0;
        try (Scanner in = new Scanner(InputLoader.getInput(2))) {
            while (in.hasNextLine()) {
                Present p = new Present(in.nextLine());
                paperTotal += p.paperSqFt();
                ribbonTotal += p.smallestPerimeter() + p.volume();
            }
        }
        System.out.println("The Elves need x sqft of paper: " + paperTotal);
        System.out.println("The Elves need x ft of ribbon: " + ribbonTotal);
    }

    static class Present {
        final int length;
        final int width;
        final int height;

        public Present(String dimensions) {
            String[] tmp = dimensions.split("x");
            length = Integer.parseInt(tmp[0]);
            width = Integer.parseInt(tmp[1]);
            height = Integer.parseInt(tmp[2]);
        }

        public int paperSqFt() {
            return area() + Math.min(Math.min(length * width, width * height), height * length);
        }

        public int area() {
            return 2 * length * width + 2 * width * height + 2 * height * length;
        }

        public int volume() {
            return length * width * height;
        }

        public int smallestPerimeter() {
            return 2 * (length + width + height - Math.max(Math.max(length, width), height));
        }
    }

    public void doDay3() {
        Map<House, Integer> housesVisited = new HashMap<>();
        int xLoc = 0;
        int yLoc = 0;
        housesVisited.put(new House(xLoc, yLoc), 1);
        try (InputStream in = InputLoader.getInput(3)) {
            while (in.available() > 0) {
                char instruction = (char) in.read();
                switch (instruction) {
                    case '>':
                        xLoc++;
                        break;
                    case '<':
                        xLoc--;
                        break;
                    case '^':
                        yLoc++;
                        break;
                    case 'v':
                        yLoc--;
                        break;
                    default:
                        LOG.warn("Unknown input: " + instruction);
                }
                House house = new House(xLoc, yLoc);
                int visits = housesVisited.getOrDefault(house, 0);
                // LOG.debug("{} went to {} visited {} times", instruction, house, visits + 1);
                housesVisited.put(house, visits + 1);
            }
        } catch (IOException e) {
            LOG.error("Problem with input", e);
            throw new RuntimeException("Problem with input", e);
        }
        System.out.println(housesVisited.keySet().size() + " houses got at least one present");
    }

    public void doDay3Part2() {
        Map<House, Integer> housesVisited = new HashMap<>();
        int santaX = 0;
        int santaY = 0;
        int robotX = 0;
        int robotY = 0;
        housesVisited.put(new House(santaX, santaY), 2);
        boolean santasTurn = true;
        try (InputStream in = InputLoader.getInput(3)) {
            while (in.available() > 0) {
                char instruction = (char) in.read();
                switch (instruction) {
                    case '>':
                        if (santasTurn)
                            santaX++;
                        else
                            robotX++;
                        break;
                    case '<':
                        if (santasTurn)
                            santaX--;
                        else
                            robotX--;
                        break;
                    case '^':
                        if (santasTurn)
                            santaY++;
                        else
                            robotY++;
                        break;
                    case 'v':
                        if (santasTurn)
                            santaY--;
                        else
                            robotY--;
                        break;
                    default:
                        LOG.warn("Unknown input: " + instruction);
                }
                House house = new House(santasTurn ? santaX : robotX, santasTurn ? santaY : robotY);
                int visits = housesVisited.getOrDefault(house, 0);
                // LOG.debug("{} went to {} visited {} times", instruction, house, visits + 1);
                housesVisited.put(house, visits + 1);
                santasTurn = !santasTurn;
            }
        } catch (IOException e) {
            LOG.error("Problem with input", e);
            throw new RuntimeException("Problem with input", e);
        }
        System.out.println(housesVisited.keySet().size() + " houses got at least one present");
    }

    static class House {
        final int x;
        final int y;

        public House(int x, int y) {
            this.x = x;
            this.y = y;
        }

        @Override
        public int hashCode() {
            final int prime = 31;
            int result = 1;
            result = prime * result + x;
            result = prime * result + y;
            return result;
        }

        @Override
        public boolean equals(Object obj) {
            if (this == obj)
                return true;
            if (obj == null)
                return false;
            if (getClass() != obj.getClass())
                return false;
            House other = (House) obj;
            if (x != other.x)
                return false;
            if (y != other.y)
                return false;
            return true;
        }

        @Override
        public String toString() {
            return "House [x=" + x + ", y=" + y + "]";
        }
    }

    public void doDay4() {
        doDay4(false);
    }

    public void doDay4(boolean part2) {
        String startCheck = part2 ? "000000" : "00000";
        String secret;
        try (Scanner in = new Scanner(InputLoader.getInput(4))) {
            secret = in.nextLine();
        }
        int threads = 64;
        Instant start = Instant.now();
        final int[] attempt = { 0 };
        while (true) {
            OptionalInt answer = IntStream.range(0, threads)// .parallel()
                    .map(x -> {
                        if (DigestUtils.md5Hex(secret + (attempt[0] * threads + x)).startsWith(startCheck)) {
                            return x;
                        } else {
                            return -1;
                        }
                    }).filter(x -> x >= 0).findFirst();
            if (answer.isPresent()) {
                System.out.println("Answer is " + (attempt[0] * threads + answer.getAsInt()));
                break;
            }
            attempt[0]++;
        }
        System.out.println("Took " + Duration.between(start, Instant.now()).toMillis() + "ms");
    }

    public void doDay5() {
        // int niceStrings = 0;
        Pattern vowels = Pattern.compile("(?i)[aeiou].*[aeiou].*[aeiou]");
        Pattern doubleLetter = Pattern.compile("(.)\\1");
        BufferedReader br = new BufferedReader(new InputStreamReader(InputLoader.getInput(5)));
        System.out.println("Nice lines: " + br.lines().filter(line -> {
            return !line.contains("ab") && !line.contains("cd") && !line.contains("pq") && !line.contains("xy")
                    && vowels.asPredicate().test(line) && doubleLetter.asPredicate().test(line);
        }).count());
        try {
            br.close();
        } catch (IOException e) {
            // Don't care
        }
    }

    public void doDay5Part2() {
        Pattern pair = Pattern.compile("(..).*\\1");
        Pattern repeat = Pattern.compile("(.).\\1");
        BufferedReader br = new BufferedReader(new InputStreamReader(InputLoader.getInput(5)));
        System.out.println("Nice lines: " + br.lines().filter(pair.asPredicate()).filter(repeat.asPredicate()).count());
        try {
            br.close();
        } catch (IOException e) {
            // Don't care
        }
    }

    public void doDay6() {
        Pattern parser = Pattern
                .compile("(?<operation>.+) (?<xmin>\\d+),(?<ymin>\\d+) through (?<xmax>\\d+),(?<ymax>\\d+)");
        boolean[][] lightGrid = new boolean[1000][1000];
        try (Scanner in = new Scanner(InputLoader.getInput(6))) {
            while (in.hasNextLine()) {
                Matcher m = parser.matcher(in.nextLine());
                if (!m.matches()) {
                    LOG.warn("Unmatched line: " + m.group());
                }
                for (int x = Integer.parseInt(m.group("xmin")); x <= Integer.parseInt(m.group("xmax")); ++x) {
                    for (int y = Integer.parseInt(m.group("ymin")); y <= Integer.parseInt(m.group("ymax")); ++y) {
                        String op = m.group("operation");
                        if (op.endsWith("on")) {
                            lightGrid[x][y] = true;
                        } else if (op.endsWith("off")) {
                            lightGrid[x][y] = false;
                        } else if (op.endsWith("gle")) {
                            lightGrid[x][y] = !lightGrid[x][y];
                        } else {
                            LOG.warn("Unknown operation: " + op + " for line: " + m.group());
                        }
                    }
                }
            }
        }
        // Count "on" lights
        System.out.println("Lights that are lit: " + Arrays.stream(lightGrid)
                .map(row -> IntStream.range(0, row.length).filter(i -> row[i]).count()).reduce((a, b) -> a + b).get());
    }

    public void doDay6Part2() {
        Pattern parser = Pattern
                .compile("(?<operation>.+) (?<xmin>\\d+),(?<ymin>\\d+) through (?<xmax>\\d+),(?<ymax>\\d+)");
        int[][] lightGrid = new int[1000][1000];
        try (Scanner in = new Scanner(InputLoader.getInput(6))) {
            while (in.hasNextLine()) {
                Matcher m = parser.matcher(in.nextLine());
                if (!m.matches()) {
                    LOG.warn("Unmatched line: " + m.group());
                }
                for (int x = Integer.parseInt(m.group("xmin")); x <= Integer.parseInt(m.group("xmax")); ++x) {
                    for (int y = Integer.parseInt(m.group("ymin")); y <= Integer.parseInt(m.group("ymax")); ++y) {
                        String op = m.group("operation");
                        if (op.endsWith("on")) {
                            lightGrid[x][y]++;
                        } else if (op.endsWith("off")) {
                            lightGrid[x][y]--;
                            if (lightGrid[x][y] < 0) {
                                lightGrid[x][y] = 0;
                            }
                        } else if (op.endsWith("gle")) {
                            lightGrid[x][y] += 2;
                        } else {
                            LOG.warn("Unknown operation: " + op + " for line: " + m.group());
                        }
                    }
                }
            }
        }
        // Count "on" lights
        System.out.println("Total light brightness: " + Arrays.stream(lightGrid)
                .map(row -> Arrays.stream(row).reduce((a, b) -> a + b).getAsInt()).reduce((a, b) -> a + b).get());
    }

    public void doDay7() {
        doDay7(null);
    }

    public void doDay7(Integer part2) {
        Pattern namedWire = Pattern.compile("[A-z]+");
        Pattern parser = Pattern.compile(
                "(?:(?<wire1>[A-z]+|[0-9]+) )?(?:(?<operation>[A-Z]+) (?<wire2>[A-z]+|[0-9]+) )?-> (?<output>[A-z]+)");
        Map<String, Wire> wires = new TreeMap<>();
        try (Scanner in = new Scanner(InputLoader.getInput(7))) {
            while (in.hasNextLine()) {
                // parse
                Matcher m = parser.matcher(in.nextLine());
                if (!m.matches()) {
                    LOG.error("Bad input line: " + m.group());
                }
                // create wires
                Wire output = wires.computeIfAbsent(m.group("output"), Wire::new);
                String wire1Name = m.group("wire1");
                String wire2Name = m.group("wire2");
                // normal operations
                if (wire2Name != null) {
                    if (!namedWire.matcher(wire2Name).matches()) {
                        wires.computeIfAbsent(wire2Name, k -> {
                            Wire wire = new Wire(k);
                            wire.setValue(Short.toUnsignedInt(Short.valueOf(wire2Name)));
                            return wire;
                        });
                    }
                    Gate gate = new Gate(Gate.Operation.valueOf(m.group("operation")),
                            wire1Name == null ? null : wires.computeIfAbsent(wire1Name, Wire::new),
                            wires.computeIfAbsent(wire2Name, Wire::new));
                    output.setValue(gate);
                } else { // wire connection
                    if (part2 != null && output.name.equals("b")) {
                        System.out.println("Overriding " + wire1Name + " with " + part2);
                        wire1Name = String.valueOf((short) (part2 & 0xffff));
                    }
                    if (namedWire.matcher(wire1Name).matches()) {
                        output.setValue(wires.computeIfAbsent(wire1Name, Wire::new));
                    } else { // raw value
                        output.setValue(Short.toUnsignedInt(Short.valueOf(wire1Name)));
                    }
                }
            }
        }
        System.out.println("The signal on A is: " + (wires.get("a").getValue() & 0xffff));
        if (part2 == null) {
            doDay7(wires.get("a").getValue() & 0xffff);
        }
    }

    private static class Gate {
        enum Operation {
            AND((a, b) -> a & b), OR((a, b) -> a | b), LSHIFT((a, b) -> a << b & 0xffff), RSHIFT((a, b) -> a >> b),
            NOT((a, b) -> ((~b) & 0xffff));

            BinaryOperator<Integer> calculation;

            Operation(BinaryOperator<Integer> operation) {
                calculation = operation;
            }
        }

        private final Operation op;
        private final Wire input1;
        private final Wire input2;

        private Integer calculatedValue;

        Gate(Operation op, Wire input1, Wire input2) {
            this.op = op;
            this.input1 = input1;
            this.input2 = input2;
        }

        Integer getValue() {
            LOG.trace("Getting {}", this);
            if (calculatedValue == null
                    && Stream.of(input1, input2).allMatch(wire -> wire == null || wire.hasValue())) {
                calculatedValue = op.calculation.apply(input1 == null ? null : input1.getValue(), input2.getValue());
            }
            return calculatedValue;
        }

        public String toString() {
            return "Gate [" + (input1 != null ? input1.name + " " : "") + op + " " + input2.name + "]";
        }
    }

    private static class Wire {
        String name;
        Integer value;
        Gate gateSource;
        Wire wireSource;

        Wire(String name) {
            this.name = name;
        }

        boolean hasValue() {
            return value != null || gateSource != null || wireSource != null;
        }

        private void assertUnSet() {
            if (hasValue()) {
                throw new RuntimeException(this + " already has a value");
            }
        }

        void setValue(int raw) {
            assertUnSet();
            value = raw;
        }

        void setValue(Gate gate) {
            assertUnSet();
            gateSource = gate;
        }

        void setValue(Wire wire) {
            assertUnSet();
            wireSource = wire;
        }

        Integer getValue() {
            // Self memoize
            LOG.trace("Getting {}", this);
            if (gateSource != null) {
                value = gateSource.getValue();
                gateSource = null;
            } else if (wireSource != null) {
                value = wireSource.getValue();
                wireSource = null;
            }
            LOG.trace("Got {}", this);
            if (value != null) {
                return value;
            } else {
                throw new RuntimeException("Can't get value from unset wire");
            }
        }

        @Override
        public String toString() {
            return value != null ? String.valueOf(value & 0xffff)
                    : "Wire [" + name + "("
                            + (gateSource != null ? gateSource : wireSource != null ? wireSource : "Unset") + ")]";
        }

    }

    public void doDay8() {
        long rawCharacters = 0;
        long memoryCharacters = 0;
        Pattern hex = Pattern.compile("(?i)\\\\x(?<hex>[0-9a-f][0-9a-f])");
        try (Scanner in = new Scanner(InputLoader.getInput(8))) {
            while (in.hasNextLine()) {
                String line = in.nextLine();
                rawCharacters += line.length();
                line = line.replaceAll("\\\\\"", "\"").replaceAll("\\\\\\\\", "\\\\").replaceFirst("^\"", "")
                        .replaceFirst("\"$", "");
                Matcher m = hex.matcher(line);
                while (m.find()) {
                    line = m.replaceFirst("" + (char) Integer.parseInt(m.group("hex"), 16));
                    m = hex.matcher(line);
                }
                memoryCharacters += line.length();
            }
        }
        System.out.println("Raw chars: " + rawCharacters);
        System.out.println("Mem chars: " + memoryCharacters);
        System.out.println("Difference: " + (rawCharacters - memoryCharacters));
    }

    public void doDay8Part2() {
        long rawCharacters = 0;
        long encodedCharacters = 0;
        try (Scanner in = new Scanner(InputLoader.getInput(8))) {
            while (in.hasNextLine()) {
                String line = in.nextLine();
                rawCharacters += line.length();
                System.out.println(line);
                line = "\"" + line.replaceAll("\\\\", "\\\\\\\\").replaceAll("\"", "\\\\\"") + "\"";
                System.out.println(line);
                encodedCharacters += line.length();
            }
        }
        System.out.println("Raw chars: " + rawCharacters);
        System.out.println("Enc chars: " + encodedCharacters);
        System.out.println("Difference: " + (encodedCharacters - rawCharacters));
    }

    public void doDay9() {
        doDay9(false);
    }

    public void doDay9(boolean part2) {
        Set<String> cities = new HashSet<>();
        Map<String, Trip> trips = new HashMap<>();
        BinaryOperator<Trip> shortest = (a, b) -> a.distance < b.distance ? a : b;
        BinaryOperator<Trip> longest = (a, b) -> a.distance > b.distance ? a : b;

        try (Scanner in = new Scanner(InputLoader.getInput(9))) {
            while (in.hasNextLine()) {
                String[] info = in.nextLine().split("\s+");
                cities.add(info[0]);
                cities.add(info[2]);
                trips.put(info[0] + " -> " + info[2], new Trip(Integer.parseInt(info[4]), info[0], info[2]));
                // And reverse trip
                trips.put(info[2] + " -> " + info[0], new Trip(Integer.parseInt(info[4]), info[2], info[0]));
            }
        }
        for (int i = 2; i <= (int) Math.ceil(cities.size() / 2); ++i) {
            findNextCity(cities, trips, i, part2 ? longest : shortest);
        }
        int firstSize = (int) Math.ceil(cities.size() / 2);
        int secondSize = (int) Math.floor(cities.size() / 2) + 1;

        combineTrips(trips, firstSize, secondSize);

        Trip shortestTrip = trips.values().stream().filter(trip -> trip.citiesVisited.size() == cities.size())
                .reduce(part2 ? longest : shortest).get();
        System.out.println("Shortest trip is " + shortestTrip);
    }

    void findNextCity(Set<String> cities, Map<String, Trip> trips, int tripSize, BinaryOperator<Trip> mergeFunction) {
        trips.putAll(trips.values().stream().filter(trip -> trip.count() == tripSize).map(trip -> {
            return cities.stream().filter(city -> !trip.visited(city)).map(city -> {
                int distance = trip.distance + trips.get(trip.last + " -> " + city).distance;
                Trip newTrip = trip.copy();
                newTrip.distance = distance;
                newTrip.last = city;
                newTrip.citiesVisited.add(city);
                return newTrip;
            });
        }).flatMap(s -> s).collect(Collectors.toMap(
                trip -> trip.citiesVisited.stream().collect(Collectors.joining(" -> ")), trip -> trip, mergeFunction)));
    }

    void combineTrips(Map<String, Trip> trips, int firstSize, int secondSize) {
        trips.putAll(trips.values().stream().filter(trip -> trip.citiesVisited.size() == firstSize).map(firstTrip -> {
            return trips.values().stream().filter(secondTrip -> secondTrip.citiesVisited.size() == secondSize)
                    .filter(secondTrip -> secondTrip.startsWith(firstTrip.last)).map(secondTrip -> {
                        Trip fullTrip = firstTrip.copy();
                        fullTrip.distance = firstTrip.distance + secondTrip.distance;
                        fullTrip.citiesVisited.addAll(secondTrip.citiesVisited);
                        return fullTrip;
                    });
        }).flatMap(s -> s)
                .collect(Collectors.toMap(trip -> trip.citiesVisited.stream().collect(Collectors.joining(" -> ")),
                        trip -> trip, (a, b) -> a.distance < b.distance ? a : b)));
    }

    static class Trip {
        String first;
        String last;
        Set<String> citiesVisited;
        int distance;

        Trip(int distance, String... cities) {
            this.distance = distance;
            citiesVisited = new LinkedHashSet<>(Arrays.asList(cities));
            first = cities[0];
            last = cities[cities.length - 1];
        }

        Trip copy() {
            return new Trip(distance, citiesVisited.toArray(new String[citiesVisited.size()]));
        }

        int count() {
            return citiesVisited.size();
        }

        boolean visited(String city) {
            return citiesVisited.contains(city);
        }

        boolean startsWith(String city) {
            return first.equals(city);
        }

        boolean endsWith(String city) {
            return last.equals(city);
        }

        @Override
        public int hashCode() {
            return citiesVisited.hashCode();
        }

        @Override
        public boolean equals(Object obj) {
            if (this == obj)
                return true;
            if (obj == null)
                return false;
            if (getClass() != obj.getClass())
                return false;
            Trip other = (Trip) obj;
            if (citiesVisited == null) {
                if (other.citiesVisited != null)
                    return false;
            } else if (!citiesVisited.equals(other.citiesVisited))
                return false;
            return true;
        }

        @Override
        public String toString() {
            return "Trip [citiesVisited=" + citiesVisited.stream().collect(Collectors.joining(" -> ")) + ", distance="
                    + distance + "]";
        }
    }

    public void doDay10() {
        String input;
        try (Scanner in = new Scanner(InputLoader.getInput(10))) {
            input = in.nextLine();
        }
        int iterations = 40;
        for (int i = 0; i < iterations; ++i) {
            char[] prev = { 0 };
            int[] count = { 0 };
            StringBuilder next = new StringBuilder();
            input.chars().forEachOrdered(c -> {
                if (c != prev[0]) {
                    if (prev[0] != 0) {
                        next.append(count[0] + "" + prev[0]);
                    }
                    prev[0] = (char) c;
                    count[0] = 0;
                }
                count[0]++;
            });
            if (prev[0] != -1) {
                next.append(count[0] + "" + Character.toString(prev[0]));
            }
            input = next.toString();
        }
        // System.out.println("Result: " + input);
        System.out.println("Length: " + input.length());
    }

    public void doDay11() {
        doDay11(null);
    }

    public void doDay11(String prev) {
        // OMG corporate policies like this one are terrible lol
        Predicate<String> hasStraight = a -> IntStream.range(0, a.length() - 2)
                .mapToObj(i -> a.charAt(i) == a.charAt(i + 1) - 1 && a.charAt(i) == a.charAt(i + 2) - 2)
                .anyMatch(b -> b);
        Pattern invalidChars = Pattern.compile("[iol]");
        Pattern pairs = Pattern.compile("([a-z])\\1.*([a-z])(?!\\1)\\2");
        Predicate<String> validPassword = a -> hasStraight.test(a) && !invalidChars.matcher(a).find()
                && pairs.matcher(a).find();

        String input;
        if (prev == null) {
            try (Scanner in = new Scanner(InputLoader.getInput(11))) {
                input = in.nextLine();
            }
        } else {
            input = prev;
        }
        do {
            int pos = input.length();
            char[] iter = input.toCharArray();
            do {
                pos--;
                iter[pos]++;
                if (iter[pos] == ('z' + 1)) {
                    iter[pos] = 'a';
                }
            } while (iter[pos] == 'a');
            input = new String(iter);
        } while (!validPassword.test(input));
        System.out.println("Next password: " + input);
        if (prev == null) {
            doDay11(input);
        }
    }

    public void doDay12() {
        doDay12(false);
    }

    public void doDay12(boolean part2) {
        String input;
        try (Scanner in = new Scanner(InputLoader.getInput(12))) {
            input = in.nextLine();
        }
        Object json = new JSONTokener(input).nextValue();
        System.out.println("Sum in JSON is: " + sumJSONNumbers(part2, json));
    }

    int sumJSONNumbers(boolean part2, Object obj) {
        if (obj instanceof JSONObject) {
            JSONObject json = (JSONObject) obj;
            if (part2 && json.keySet().stream().anyMatch(key -> json.get(key).equals("red"))) {
                return 0;
            }
            return json.keySet().stream().map(item -> sumJSONNumbers(part2, json.get(item))).reduce(Integer::sum)
                    .orElseGet(() -> 0);
        } else if (obj instanceof JSONArray) {
            JSONArray json = (JSONArray) obj;
            return StreamSupport.stream(json.spliterator(), false).map(item -> sumJSONNumbers(part2, item))
                    .reduce(Integer::sum).orElseGet(() -> 0);
        } else if (obj instanceof String) {
            // ignore
            return 0;
        } else if (obj instanceof Number) {
            return ((Number) obj).intValue();
        } else {
            LOG.warn("Unknown type: {}" + obj.getClass());
            return 0;
        }
    }

    public void doDay13() {
        Pattern parser = Pattern
                .compile("^(?<person>\\w+) would (?<sign>gain|lose) (?<amount>\\d+) happ.*to (?<sibling>\\w+)\\.$");
        Map<String, Person> people = new HashMap<>();

        try (Scanner in = new Scanner(InputLoader.getInput(13))) {
            while (in.hasNextLine()) {
                Matcher m = parser.matcher(in.nextLine());
                if (!m.matches()) {
                    LOG.warn("Bad input line: " + m.group());
                }
                Person person = people.computeIfAbsent(m.group("person"), Person::new);
                int amount = (m.group("sign").equals("lose") ? -1 : 1) * Integer.parseInt(m.group("amount"));
                person.relationships.put(people.computeIfAbsent(m.group("sibling"), Person::new), amount);
            }
        }

        // Compute possible seating arrangements
        int count1 = (int) Math.floor(people.keySet().size() / 2) + 1;
        int count2 = (int) Math.ceil(people.keySet().size() / 2) + 1;

        Map<Integer, Set<LinkedList<Person>>> arrangements = new HashMap<>();
        arrangements.put(1, people.values().stream()
                .map(p -> Stream.of(p).collect(Collectors.toCollection(LinkedList::new))).collect(Collectors.toSet()));
        IntStream.rangeClosed(2, count2).forEach(i -> {
            arrangements.put(i, arrangements.get(i - 1).stream().map(l -> {
                Set<Person> newPeople = new HashSet<>(people.values());
                newPeople.removeAll(l);
                return newPeople.stream().map(p -> {
                    LinkedList<Person> newArrangement = new LinkedList<>(l);
                    newArrangement.add(p);
                    return newArrangement;
                });
            }).flatMap(Function.identity()).collect(Collectors.toSet()));
        });

        // Join seating halves according to count1 and count2
        Set<LinkedList<Person>> set1 = arrangements.get(count1);
        Set<LinkedList<Person>> set2 = arrangements.get(count2);
        arrangements.put(people.size(), set1.stream().map(l1 -> {
            return set2.stream()
                    // uniqueness
                    .filter(l2 -> {
                        LinkedList<Person> l1Test = new LinkedList<>(l1);
                        l1Test.removeAll(l2);
                        return l1Test.size() == count1 - 2;
                    })
                    // first match
                    .filter(l2 -> l2.getFirst().equals(l1.getLast()))
                    // last match
                    .filter(l2 -> l2.getLast().equals(l1.getFirst())).map(l2 -> {
                        LinkedList<Person> lNew = new LinkedList<>(l2);
                        lNew.removeLast();
                        lNew.removeFirst();
                        lNew.addAll(l1);
                        return lNew;
                    });
        }).flatMap(Function.identity()).collect(Collectors.toSet()));

        // Find happiness of arrangements
        Map<Integer, LinkedList<Person>> happiness = arrangements.get(people.size()).stream()
                .collect(Collectors.toMap(arrangement -> IntStream.range(0, arrangement.size())
                        .map(i -> arrangement.get(i).happiness(
                                arrangement.get(Math.floorMod(i - 1, arrangement.size())),
                                arrangement.get(Math.floorMod(i + 1, arrangement.size()))))
                        .reduce(Integer::sum).getAsInt(), Function.identity(), (a, b) -> b));

        int max = happiness.keySet().stream().mapToInt(v -> v).max().getAsInt();
        System.out.println("The max happiness is: " + max);
        System.out.println("The arrangement is: "
                + happiness.get(max).stream().map(Person::getName).collect(Collectors.joining(", ")));

        // Part 2
        Person me = people.computeIfAbsent("Me", Person::new);
        people.values().stream().filter(p -> p != me).forEach(p -> {
            me.relationships.put(p, 0);
            p.relationships.put(me, 0);
        });

        // Add me in every max size seating arrangement in every position
        arrangements.put(people.size(),
                arrangements.get(people.size() - 1).stream().map(l -> IntStream.rangeClosed(0, l.size()).mapToObj(i -> {
                    LinkedList<Person> lm = new LinkedList<>(l);
                    lm.add(i, me);
                    return lm;
                })).flatMap(Function.identity()).collect(Collectors.toSet()));

        // Find happiness of arrangements including me
        Map<Integer, LinkedList<Person>> happinessWithMe = arrangements.get(people.size()).stream()
                .collect(Collectors.toMap(arrangement -> IntStream.range(0, arrangement.size())
                        .map(i -> arrangement.get(i).happiness(
                                arrangement.get(Math.floorMod(i - 1, arrangement.size())),
                                arrangement.get(Math.floorMod(i + 1, arrangement.size()))))
                        .reduce(Integer::sum).getAsInt(), Function.identity(), (a, b) -> b));

        int maxWithMe = happinessWithMe.keySet().stream().mapToInt(v -> v).max().getAsInt();
        System.out.println("The max happiness is: " + maxWithMe);
        System.out.println("The arrangement is: "
                + happinessWithMe.get(maxWithMe).stream().map(Person::getName).collect(Collectors.joining(", ")));
    }

    static class Person {
        final String name;
        Map<Person, Integer> relationships = new HashMap<>();

        Person(String name) {
            this.name = name;
        }

        int happiness(Person leftNeighbor, Person rightNeighbor) {
            return (leftNeighbor != null ? relationships.get(leftNeighbor) : 0)
                    + (rightNeighbor != null ? relationships.get(rightNeighbor) : 0);
        }

        String getName() {
            return name;
        }

        @Override
        public int hashCode() {
            final int prime = 31;
            int result = 1;
            result = prime * result + ((name == null) ? 0 : name.hashCode());
            return result;
        }

        @Override
        public boolean equals(Object obj) {
            if (this == obj)
                return true;
            if (obj == null)
                return false;
            if (getClass() != obj.getClass())
                return false;
            Person other = (Person) obj;
            if (name == null) {
                if (other.name != null)
                    return false;
            } else if (!name.equals(other.name))
                return false;
            return true;
        }
    }

    public void doDay14() {
        int raceSeconds = 2503;
        Pattern parser = Pattern.compile(
                "(?<name>\\w+) can fly (?<speed>\\d+) km/s for (?<flytime>\\d+) seconds.*for (?<resttime>\\d+) seconds.");
        Set<Reindeer> reindeer = new HashSet<>();
        try (Scanner in = new Scanner(InputLoader.getInput(14))) {
            while (in.hasNextLine()) {
                Matcher m = parser.matcher(in.nextLine());
                if (!m.matches()) {
                    LOG.warn("Bad input: " + m.group());
                }
                reindeer.add(new Reindeer(m.group("name"), Integer.parseInt(m.group("speed")),
                        Integer.parseInt(m.group("flytime")), Integer.parseInt(m.group("resttime"))));
            }
        }

        // Do the race in seconds
        for (int i = 0; i < raceSeconds; ++i) {
            reindeer.stream().forEach(Reindeer::tick);
            int longestDistance = reindeer.stream().reduce((r1, r2) -> r1.distance > r2.distance ? r1 : r2)
                    .get().distance;
            // Ties all get points
            reindeer.stream().filter(r -> r.distance == longestDistance).forEach(r -> r.points++);
        }

        Reindeer distanceWinner = reindeer.stream().reduce((r1, r2) -> r1.distance > r2.distance ? r1 : r2).get();
        System.out.println("The winning reindeer was: " + distanceWinner.name + " with a distance of "
                + distanceWinner.distance + " km");
        Reindeer pointWinner = reindeer.stream().reduce((r1, r2) -> r1.points > r2.points ? r1 : r2).get();
        System.out.println("The winning reindeer was: " + pointWinner.name + " with " + pointWinner.points + " points");
    }

    static class Reindeer {
        static enum State {
            FLYING, RESTING;
        }

        final String name;
        final int speed;
        final int flyTime;
        final int restTime;

        State state = State.FLYING;
        int stateTime;
        int distance = 0;
        int points = 0;

        public Reindeer(String name, int speed, int flyTime, int restTime) {
            this.name = name;
            this.speed = speed;
            this.flyTime = flyTime;
            stateTime = flyTime;
            this.restTime = restTime;
        }

        public void tick() {
            stateTime--;
            switch (state) {
                case FLYING:
                    distance += speed;
                    if (stateTime == 0) {
                        state = State.RESTING;
                        stateTime = restTime;
                    }
                    break;
                case RESTING:
                    if (stateTime == 0) {
                        state = State.FLYING;
                        stateTime = flyTime;
                    }
                    break;
            }
        }

        @Override
        public int hashCode() {
            final int prime = 31;
            int result = 1;
            result = prime * result + ((name == null) ? 0 : name.hashCode());
            return result;
        }

        @Override
        public boolean equals(Object obj) {
            if (this == obj)
                return true;
            if (obj == null)
                return false;
            if (getClass() != obj.getClass())
                return false;
            Reindeer other = (Reindeer) obj;
            if (name == null) {
                if (other.name != null)
                    return false;
            } else if (!name.equals(other.name))
                return false;
            return true;
        }
    }

    public void doDay15() {
        Pattern parser = Pattern.compile(
                "(?<name>\\w+): capacity (?<capacity>-?\\d+), durability (?<durability>-?\\d+), flavor (?<flavor>-?\\d+), texture (?<texture>-?\\d+), calories (?<calories>-?\\d+)");
        List<Ingredient> ingredients = new ArrayList<>();
        try (Scanner in = new Scanner(InputLoader.getInput(15))) {
            while (in.hasNextLine()) {
                Matcher m = parser.matcher(in.nextLine());
                if (!m.matches()) {
                    LOG.warn("Bad input: " + m.group());
                }
                ingredients.add(new Ingredient(m.group("name"), Integer.parseInt(m.group("capacity")),
                        Integer.parseInt(m.group("durability")), Integer.parseInt(m.group("flavor")),
                        Integer.parseInt(m.group("texture")), Integer.parseInt(m.group("calories"))));
            }
        }
        // Optimization problem
        // combine counts of ingredients, counts total 100
        Set<List<Integer>> ingredCounts = buildIngredientCounts(ingredients.size(), 100);
        Map<Integer, List<Integer>> scores = getScores(ingredients, ingredCounts);
        int max = scores.keySet().stream().mapToInt(Integer::intValue).max().getAsInt();
        System.out.println("The max score was: " + max);
        System.out.println("With ingredients: " + IntStream.range(0, ingredients.size())
                .mapToObj(i -> scores.get(max).get(i) + " " + ingredients.get(i).name)
                .collect(Collectors.joining(", ")));

        // Filter recipes by calorie count
        Map<Integer, List<Integer>> caloriedScores = getScores(ingredients,
                ingredCounts.stream().filter(
                        l -> IntStream.range(0, l.size()).map(i -> ingredients.get(i).calories * l.get(i)).sum() == 500)
                        .collect(Collectors.toSet()));
        int caloriedMax = caloriedScores.keySet().stream().mapToInt(Integer::intValue).max().getAsInt();
        System.out.println("The max score for 500 calorie cookies was: " + caloriedMax);
        System.out.println("With ingredients: " + IntStream.range(0, ingredients.size())
                .mapToObj(i -> caloriedScores.get(caloriedMax).get(i) + " " + ingredients.get(i).name)
                .collect(Collectors.joining(", ")));
    }

    public Map<Integer, List<Integer>> getScores(List<Ingredient> ingredients, Set<List<Integer>> ingredCounts) {
        return ingredCounts.stream().collect(Collectors.toMap(l -> // Get score
        Math.max(0, IntStream.range(0, l.size()).map(i -> ingredients.get(i).capacity * l.get(i)).sum())
                * Math.max(0, IntStream.range(0, l.size()).map(i -> ingredients.get(i).durability * l.get(i)).sum())
                * Math.max(0, IntStream.range(0, l.size()).map(i -> ingredients.get(i).flavor * l.get(i)).sum())
                * Math.max(0, IntStream.range(0, l.size()).map(i -> ingredients.get(i).texture * l.get(i)).sum()),
                Function.identity(), (a, b) -> a));
    }

    public Set<List<Integer>> buildIngredientCounts(int ingredsLeft, int total) {
        if (ingredsLeft == 1) {
            return Set.of(Stream.of(total).collect(Collectors.toList()));
        }
        return IntStream.rangeClosed(0, total)
                .mapToObj(i -> buildIngredientCounts(ingredsLeft - 1, total - i).stream().peek(l -> l.add(i)))
                .flatMap(Function.identity()).collect(Collectors.toSet());
    }

    static class Ingredient {
        final String name;
        final int capacity;
        final int durability;
        final int flavor;
        final int texture;
        final int calories;

        public Ingredient(String name, int capacity, int durability, int flavor, int texture, int calories) {
            this.name = name;
            this.capacity = capacity;
            this.durability = durability;
            this.flavor = flavor;
            this.texture = texture;
            this.calories = calories;
        }

        @Override
        public int hashCode() {
            final int prime = 31;
            int result = 1;
            result = prime * result + ((name == null) ? 0 : name.hashCode());
            return result;
        }

        @Override
        public boolean equals(Object obj) {
            if (this == obj)
                return true;
            if (obj == null)
                return false;
            if (getClass() != obj.getClass())
                return false;
            Ingredient other = (Ingredient) obj;
            if (name == null) {
                if (other.name != null)
                    return false;
            } else if (!name.equals(other.name))
                return false;
            return true;
        }
    }

    public void doDay16() {
        Pattern parser = Pattern.compile("Sue (?<num>\\d+): (?:(?:children: (?<children>\\d+)|cats: (?<cats>\\d+)|samoyeds: (?<samoyeds>\\d+)|pomeranians: (?<pomeranians>\\d+)|akitas: (?<akitas>\\d+)|vizslas: (?<vizslas>\\d+)|goldfish: (?<goldfish>\\d+)|trees: (?<trees>\\d+)|cars: (?<cars>\\d+)|perfumes: (?<perfumes>\\d+))(?:, )?)+");
        Set<AuntSue> aunties = new HashSet<>();
        try (Scanner in = new Scanner(InputLoader.getInput(16))) {
            while (in.hasNextLine()) {
                Matcher m = parser.matcher(in.nextLine());
                if (!m.matches()) {
                    LOG.warn("Bad input: " + m.group());
                }
                int number = Integer.parseInt(m.group("num"));
                String children = m.group("children");
                String cats = m.group("cats");
                String samoyeds = m.group("samoyeds");
                String pomeranians = m.group("pomeranians");
                String akitas = m.group("akitas");
                String vizslas = m.group("vizslas");
                String goldfish = m.group("goldfish");
                String trees = m.group("trees");
                String cars = m.group("cars");
                String perfumes = m.group("perfumes");
                aunties.add(new AuntSue(number, children == null ? null : Integer.parseInt(children),
                    cats == null ? null : Integer.parseInt(cats),
                    samoyeds == null ? null : Integer.parseInt(samoyeds),
                    pomeranians == null ? null : Integer.parseInt(pomeranians),
                    akitas == null ? null : Integer.parseInt(akitas),
                    vizslas == null ? null : Integer.parseInt(vizslas),
                    goldfish == null ? null : Integer.parseInt(goldfish),
                    trees == null ? null : Integer.parseInt(trees),
                    cars == null ? null : Integer.parseInt(cars),
                    perfumes == null ? null : Integer.parseInt(perfumes)));
            }
        }
        // children: 3
        // cats: 7
        // samoyeds: 2
        // pomeranians: 3
        // akitas: 0
        // vizslas: 0
        // goldfish: 5
        // trees: 3
        // cars: 2
        // perfumes: 1
        AuntSue theAuntie = aunties.stream()
            .filter(sueFilter("children", 3, PredicateCompare.EQUALS))
            .filter(sueFilter("cats", 7, PredicateCompare.EQUALS))
            .filter(sueFilter("samoyeds", 2, PredicateCompare.EQUALS))
            .filter(sueFilter("pomeranians", 3, PredicateCompare.EQUALS))
            .filter(sueFilter("akitas", 0, PredicateCompare.EQUALS))
            .filter(sueFilter("vizslas", 0, PredicateCompare.EQUALS))
            .filter(sueFilter("goldfish", 5, PredicateCompare.EQUALS))
            .filter(sueFilter("trees", 3, PredicateCompare.EQUALS))
            .filter(sueFilter("cars", 2, PredicateCompare.EQUALS))
            .filter(sueFilter("perfumes", 1, PredicateCompare.EQUALS))
            //.peek(s -> System.out.println("Found Sue " + s.number))
            .findAny().get();
        
        System.out.println("Part 1: Aunt Sue number " + theAuntie.number + " sent the stuff");

        AuntSue theRealAuntie = aunties.stream()
            .filter(sueFilter("children", 3, PredicateCompare.EQUALS))
            .filter(sueFilter("cats", 7, PredicateCompare.GREATER))
            .filter(sueFilter("samoyeds", 2, PredicateCompare.EQUALS))
            .filter(sueFilter("pomeranians", 3, PredicateCompare.LESS))
            .filter(sueFilter("akitas", 0, PredicateCompare.EQUALS))
            .filter(sueFilter("vizslas", 0, PredicateCompare.EQUALS))
            .filter(sueFilter("goldfish", 5, PredicateCompare.LESS))
            .filter(sueFilter("trees", 3, PredicateCompare.GREATER))
            .filter(sueFilter("cars", 2, PredicateCompare.EQUALS))
            .filter(sueFilter("perfumes", 1, PredicateCompare.EQUALS))
            // .peek(s -> System.out.println("Found Sue " + s.number))
            .findAny().get();
        
        System.out.println("Part 2: Aunt Sue number " + theRealAuntie.number + " sent the stuff");
    }
    static enum PredicateCompare {
        EQUALS,
        GREATER,
        LESS;
    }
    Predicate<AuntSue> sueFilter(String field, Integer value, PredicateCompare pc) {
        return s -> {
            try {
                Field f = AuntSue.class.getDeclaredField(field);
                Integer v = (Integer)f.get(s);
                if (v == null) {
                    return true;
                }
                switch (pc) {
                    case EQUALS:
                        return value.equals(v);
                    case GREATER:
                        return value.compareTo(v) < 0;
                    case LESS:
                        return value.compareTo(v) > 0;
                }
                return false; // should never get here
            } catch (IllegalArgumentException | IllegalAccessException | NoSuchFieldException | SecurityException e) {
                LOG.error("Reflection error", e);
                return false;
            }
        };
    }
    static class AuntSue {
        final int number;
        final Integer children;
        final Integer cats;
        final Integer samoyeds;
        final Integer pomeranians;
        final Integer akitas;
        final Integer vizslas;
        final Integer goldfish;
        final Integer trees;
        final Integer cars;
        final Integer perfumes;

        public AuntSue(int number, Integer children, Integer cats, Integer samoyeds, Integer pomeranians, Integer akitas,
                Integer vizslas, Integer goldfish, Integer trees, Integer cars, Integer perfumes) {
            this.number = number;
            this.children = children;
            this.cats = cats;
            this.samoyeds = samoyeds;
            this.pomeranians = pomeranians;
            this.akitas = akitas;
            this.vizslas = vizslas;
            this.goldfish = goldfish;
            this.trees = trees;
            this.cars = cars;
            this.perfumes = perfumes;
        }

        @Override
        public int hashCode() {
            final int prime = 31;
            int result = 1;
            result = prime * result + number;
            return result;
        }

        @Override
        public boolean equals(Object obj) {
            if (this == obj)
                return true;
            if (obj == null)
                return false;
            if (getClass() != obj.getClass())
                return false;
            AuntSue other = (AuntSue) obj;
            if (number != other.number)
                return false;
            return true;
        }
    }
    public void doDay17() {
        Set<Container> containers = new HashSet<>();
        try (Scanner in = new Scanner(InputLoader.getInput(17))) {
            for (int i = 0; in.hasNextLine(); ++i) {
                containers.add(new Container(i, Integer.parseInt(in.nextLine())));
            }
        }
        Set<Set<Container>> containerArrangements = combineContainers(containers, new HashSet<>(), 150);
        LOG.info("There are {} arrangements: {}...", containerArrangements.size(), containerArrangements.stream().limit(10).collect(Collectors.toSet()));
        int minSize = containerArrangements.stream().mapToInt(s -> s.size()).min().getAsInt();
        Set<Set<Container>> minSizeArrangements = containerArrangements.stream().filter(s -> s.size() == minSize).collect(Collectors.toSet());
        LOG.info("There are {} arrangements of minimum size {}: {}", minSizeArrangements.size(), minSize, minSizeArrangements);
    }
    // TODO this is really slow. Find a more efficient method someday
    public Set<Set<Container>> combineContainers(Set<Container> allValidContainers, Set<Container> grouping, int total) {
        int capacity = grouping.stream().mapToInt(c -> c.capacity).sum();
        if (capacity == total) {
            Set<Set<Container>> ret = new HashSet<>();
            ret.add(grouping);
            return ret;
        }
        Set<Container> filteredContainers = allValidContainers.stream()
            // Get rid of any containers that would exceed the total
            .filter(c -> capacity + c.capacity <= total).collect(Collectors.toSet());
        return filteredContainers.stream().map(c -> {
                Set<Container> newGrouping = new HashSet<>(grouping);
                newGrouping.add(c);
                Set<Container> newValidContainers = new HashSet<>(filteredContainers);
                newValidContainers.remove(c);
                return combineContainers(newValidContainers, newGrouping, total);
            }).flatMap(s -> s.stream()).collect(Collectors.toSet());
    }
    static class Container {
        int index;
        int capacity;

        public Container(int index, int capacity) {
            this.index = index;
            this.capacity = capacity;
        }

        @Override
        public int hashCode() {
            final int prime = 31;
            int result = 1;
            result = prime * result + index;
            return result;
        }

        @Override
        public boolean equals(Object obj) {
            if (this == obj)
                return true;
            if (obj == null)
                return false;
            if (getClass() != obj.getClass())
                return false;
            Container other = (Container) obj;
            if (index != other.index)
                return false;
            return true;
        }

        @Override
        public String toString() {
            return capacity + "#" + index;
        }
    }

    public void doDay18() {
        int width, height;
        String input;
        try (Scanner in = new Scanner(InputLoader.getInput(18))) {
            input = in.nextLine();
            width = input.length();
            for (height = 1; in.hasNextLine(); ++height) {
                input += in.nextLine();
            }
        }
        // flipped on diagonal compared to naming, but it doesn't matter. Just x,y makes more sense
        boolean[][] originalGrid = new boolean[width][height];
        boolean[][] currentGrid = new boolean[width][height];
        for (int i = 0; i < input.length(); ++i) {
            int x = i % width;
            int y = i / height;
            originalGrid[x][y] = input.charAt(i) == '#';
            currentGrid[x][y] = originalGrid[x][y];
        }
        int steps = 100;
        for (int s = 0; s < steps; ++s) {
            boolean[][] nextGrid = new boolean[width][height];
            for (int i = 0; i < input.length(); ++i) {
                int x = i % width;
                int y = i / height;
                // Get subgrid (neighbors)
                int subwidth = x == 0 || x == width - 1 ? 2 : 3;
                int subheight = y == 0 || y == height - 1 ? 2 : 3;
                int subymin = Math.max(0, y-1);
                int subymax = subymin + subheight;
                boolean[][] neighbors = new boolean[subwidth][subheight];
                for (int j = 0, subx = Math.max(0, x-1); j < subwidth; ++j, ++subx) {
                    neighbors[j] = Arrays.copyOfRange(currentGrid[subx], subymin, subymax); 
                }
                if (currentGrid[x][y]) {
                    // Exclude "on" central light with -1
                    int onNeighbors = Arrays.stream(neighbors).mapToInt(a -> IntStream.range(0,a.length).map(b -> a[b] ? 1 : 0).sum()).sum() -1;
                    nextGrid[x][y] = onNeighbors == 2 || onNeighbors == 3;
                } else {
                    int onNeighbors1 = Arrays.stream(neighbors).mapToInt(a -> IntStream.range(0,a.length).map(b -> a[b] ? 1 : 0).sum()).sum();
                    nextGrid[x][y] = onNeighbors1 == 3;
                }
            }
            currentGrid = nextGrid;
        }
        int onCount = Arrays.stream(currentGrid).mapToInt(a -> IntStream.range(0,a.length).map(b -> a[b] ? 1 : 0).sum()).sum();
        LOG.info("The current count of on lights after {} steps is {}", steps, onCount);
        LOG.info("The grid:\n{}", Arrays.stream(currentGrid).map(row -> IntStream.range(0, row.length).mapToObj(light -> row[light] ? "#" : ".").collect(Collectors.joining())).collect(Collectors.joining("\n")));

        currentGrid = originalGrid;
        // Set the corners
        currentGrid[0][0] = true;
        currentGrid[0][height - 1] = true;
        currentGrid[width - 1][0] = true;
        currentGrid[width - 1][height - 1] = true;
        for (int s = 0; s < steps; ++s) {
            boolean[][] nextGrid = new boolean[width][height];
            for (int i = 0; i < input.length(); ++i) {
                int x = i % width;
                int y = i / height;
                // Make the corner lights stay on
                if ((x == 0 || x == width-1) && (y == 0 || y == height-1)) {
                    nextGrid[x][y] = true;
                    continue;
                }
                // Get subgrid (neighbors)
                int subwidth = x == 0 || x == width - 1 ? 2 : 3;
                int subheight = y == 0 || y == height - 1 ? 2 : 3;
                int subymin = Math.max(0, y-1);
                int subymax = subymin + subheight;
                boolean[][] neighbors = new boolean[subwidth][subheight];
                for (int j = 0, subx = Math.max(0, x-1); j < subwidth; ++j, ++subx) {
                    neighbors[j] = Arrays.copyOfRange(currentGrid[subx], subymin, subymax); 
                }
                if (currentGrid[x][y]) {
                    // Exclude "on" central light with -1
                    int onNeighbors = Arrays.stream(neighbors).mapToInt(a -> IntStream.range(0,a.length).map(b -> a[b] ? 1 : 0).sum()).sum() -1;
                    nextGrid[x][y] = onNeighbors == 2 || onNeighbors == 3;
                } else {
                    int onNeighbors1 = Arrays.stream(neighbors).mapToInt(a -> IntStream.range(0,a.length).map(b -> a[b] ? 1 : 0).sum()).sum();
                    nextGrid[x][y] = onNeighbors1 == 3;
                }
            }
            currentGrid = nextGrid;
        }
        int onCount2 = Arrays.stream(currentGrid).mapToInt(a -> IntStream.range(0,a.length).map(b -> a[b] ? 1 : 0).sum()).sum();
        LOG.info("Part 2: The current count of on lights after {} steps is {}", steps, onCount2);
        LOG.info("Part 2: The grid:\n{}", Arrays.stream(currentGrid).map(row -> IntStream.range(0, row.length).mapToObj(light -> row[light] ? "#" : ".").collect(Collectors.joining())).collect(Collectors.joining("\n")));
    }

    public void doDay19() {
        Pattern parser = Pattern.compile("^(?<original>\\w+) => (?<replacement>\\w+)$");
        Set<Replacement> replacements = new HashSet<>();
        String[] molecule = new String[1];
        try (Scanner in = new Scanner(InputLoader.getInput(19))) {
            while (in.hasNextLine()) {
                String line = in.nextLine();
                Matcher m = parser.matcher(line);
                if (m.matches()) {
                    replacements.add(new Replacement(m.group("original"), m.group("replacement")));
                } else if (!line.isEmpty()) {
                    molecule[0] = line;
                }
            }
        }
        Set<String> newMolecules = replacements.stream().filter(r -> molecule[0].contains(r.original)).map(r -> {
            Set<String> replaced = new HashSet<>();
            for (int i = molecule[0].indexOf(r.original); i != -1 && i < molecule[0].length(); i = molecule[0].indexOf(r.original, i + 1)) {
                replaced.add(molecule[0].substring(0, i) + molecule[0].substring(i).replaceFirst(r.original, r.replacement));
            }
            return replaced;
        }).flatMap(s -> s.stream()).collect(Collectors.toSet());
        LOG.info("{} unique new molecules can be created.", newMolecules.size());
    }
    public void doDay19Part2() {
        Pattern parser = Pattern.compile("^(?<original>\\w+) => (?<replacement>\\w+)$");
        Set<Replacement> replacements = new HashSet<>();
        String molecule = "";
        try (Scanner in = new Scanner(InputLoader.getInput(19))) {
            while (in.hasNextLine()) {
                String line = in.nextLine();
                Matcher m = parser.matcher(line);
                if (m.matches()) {
                    replacements.add(new Replacement(m.group("original"), m.group("replacement")));
                } else if (!line.isEmpty()) {
                    molecule = line;
                }
            }
        }
        Map<String, Integer> shortest = new HashMap<>();
        shortestUnbuild(shortest, "e", molecule, replacements, 0);
        LOG.info("{} is the fewest replacements needed.", shortest.get("e"));
    }
    // Again, slow. Probably a better way? substring memoization or something? Naive memoization isn't good... basically doesn't complete whee
    public void shortestUnbuild(Map<String, Integer> shortest, String target, String molecule, Set<Replacement> replacements, int count) {
        if (shortest.containsKey(molecule) || shortest.containsKey(target) && count >= shortest.get(target)) {
            if (shortest.get(molecule) > count) {
                shortest.put(molecule, count);
            }
            return;
        }
        shortest.put(molecule, count);
        if (target.equals(molecule) || molecule.contains(target) || replacements.stream().filter(r -> molecule.contains(r.replacement)).count() == 0) {
            return;
        }
        replacements.stream().filter(r -> molecule.contains(r.replacement)).forEach(r -> {
            for (int i = molecule.indexOf(r.replacement); i != -1 && i < molecule.length() && (!shortest.containsKey("e") || count < shortest.get("e")); i = molecule.indexOf(r.replacement, i + 1)) {
                shortestUnbuild(shortest, target, molecule.substring(0, i) + molecule.substring(i).replaceFirst(r.replacement, r.original), replacements, count + 1);
            }
        });
    }
    static class Replacement {
        String original;
        String replacement;

        public Replacement(String original, String replacement) {
            this.original = original;
            this.replacement = replacement;
        }

        @Override
        public int hashCode() {
            final int prime = 31;
            int result = 1;
            result = prime * result + ((original == null) ? 0 : original.hashCode());
            result = prime * result + ((replacement == null) ? 0 : replacement.hashCode());
            return result;
        }

        @Override
        public boolean equals(Object obj) {
            if (this == obj)
                return true;
            if (obj == null)
                return false;
            if (getClass() != obj.getClass())
                return false;
            Replacement other = (Replacement) obj;
            if (original == null) {
                if (other.original != null)
                    return false;
            } else if (!original.equals(other.original))
                return false;
            if (replacement == null) {
                if (other.replacement != null)
                    return false;
            } else if (!replacement.equals(other.replacement))
                return false;
            return true;
        }     
    }

    public void doDay20() {}
    public void doDay21() {}
    public void doDay22() {}
    public void doDay23() {}
    public void doDay24() {}
    public void doDay25() {}
}
