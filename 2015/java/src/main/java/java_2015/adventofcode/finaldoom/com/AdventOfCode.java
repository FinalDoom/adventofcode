package java_2015.adventofcode.finaldoom.com;

interface AdventOfCode {
    void doDay1();
    void doDay2();
    void doDay3();
    void doDay4();
    void doDay5();
    void doDay6();
    void doDay7();
    void doDay8();
    void doDay9();
    void doDay10();
    void doDay11();
    void doDay12();
    void doDay13();
    void doDay14();
    void doDay15();
    void doDay16();
    void doDay17();
    void doDay18();
    void doDay19();
    void doDay20();
    void doDay21();
    void doDay22();
    void doDay23();
    void doDay24();
    void doDay25();
}
