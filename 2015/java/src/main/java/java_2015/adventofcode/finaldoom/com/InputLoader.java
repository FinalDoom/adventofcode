package java_2015.adventofcode.finaldoom.com;

import java.io.BufferedInputStream;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Scanner;

import javax.net.ssl.HttpsURLConnection;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

class InputLoader {
    private static final Logger LOG = LoggerFactory.getLogger(InputLoader.class);

    private static final String year = "2015";
    private static final Path inputStore = Path.of("./input");
    private static final Path cookieStore = inputStore.resolve("cookie");

    private static String cookie;
    static {
        if (!inputStore.toFile().exists()) {
            inputStore.toFile().mkdir();
        }
        if (!cookieStore.toFile().exists()) {
            try {
                cookieStore.toFile().createNewFile();
            } catch (IOException e) {
                LOG.error("Error making cookie file " + cookieStore.toString(), e);
            }
        }
    }

    public static void checkCookie() {
        URL inputURL;
        try {
            inputURL = new URL("https://adventofcode.com/" + InputLoader.year + "/day/1/input");
        } catch (MalformedURLException e) {
            String msg = "Bad URL: https://adventofcode.com/" + InputLoader.year + "/day/1/input";
            LOG.error(msg, e);
            throw new RuntimeException(msg, e);
        }
        try {
            HttpsURLConnection connection;
            boolean verified = false;
            cookie = Files.readString(cookieStore);
            while (!verified) {
                connection = (HttpsURLConnection) inputURL.openConnection();
                connection.addRequestProperty("Cookie", "session=" + cookie);
                if (connection.getResponseCode() != 200) {
                    try (Scanner s = new Scanner(System.in)) {
                        System.out.print("Please paste the session cookie value: ");
                        cookie = s.nextLine();
                    }
                    try (BufferedWriter writer = new BufferedWriter(new FileWriter(cookieStore.toFile()))) {
                        writer.write(cookie);
                    }
                } else {
                    verified = true;
                }
            }
        } catch (IOException e) {
            String msg = "Error connecting to check cookie";
            LOG.error(msg, e);
            throw new RuntimeException(msg, e);
        }
    }

    public static InputStream getInput(int day) {
        return InputLoader.getInput(day, true);
    }

    public static InputStream getInput(int day, boolean firstPuzzle) {
        File cachedFile = inputStore.resolve("day" + day + "-" + (firstPuzzle ? "0" : "1")).toFile();
        if (!cachedFile.exists()) {
            checkCookie();
            URL inputURL;
            try {
                inputURL = new URL("https://adventofcode.com/" + InputLoader.year + "/day/" + day + "/input");
            } catch (MalformedURLException e) {
                String msg = "Bad URL: https://adventofcode.com/" + InputLoader.year + "/day/" + day + "/input";
                LOG.error(msg, e);
                throw new RuntimeException(msg, e);
            }
            try {
                HttpsURLConnection connection;
                connection = (HttpsURLConnection) inputURL.openConnection();
                connection.addRequestProperty("Cookie", "session=" + cookie);
                try (BufferedInputStream in = new BufferedInputStream(connection.getInputStream())) {
                    in.transferTo(new FileOutputStream(cachedFile));
                }
            } catch (IOException e) {
                String msg = "Couldn't get input for day " + day + (firstPuzzle ? " (first)" : " (second)") + " at "
                        + inputURL.toString();
                LOG.error(msg, e);
                throw new RuntimeException(msg, e);
            }
        }
        try {
            return new FileInputStream(cachedFile);
        } catch (FileNotFoundException e) {
            LOG.error("File doesn't exist for some reason?", e);
            throw new RuntimeException(e);
        }
    }
}