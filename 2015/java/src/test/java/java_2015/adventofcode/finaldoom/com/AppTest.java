package java_2015.adventofcode.finaldoom.com;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

/**
 * Unit test for simple App.
 */
 public class AppTest 
{
    /**
     * Rigorous Test :-)
     */
    @Test
    public void shouldAnswerWithTrue()
    {
        assertTrue( true );
    }

    @ParameterizedTest
    @ValueSource(strings = {"10 2x3x4", "4 1x1x10"})
    public void testPresent(String dimensions) {
        assertEquals(Integer.valueOf(dimensions.split(" ")[0]), new App.Present(dimensions.split(" ")[1]).smallestPerimeter());
    }
}
